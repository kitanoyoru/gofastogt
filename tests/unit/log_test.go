package unittests

import (
	"errors"
	"log"
	"os"
	"testing"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Test func InitLogFile
func TestLogFileSizeMax(t *testing.T) {
	var maxFileSize int64 = 100
	pathFile, err := gofastogt.StableFilePath("~/test.log")
	if err != nil {
		t.Errorf("StableFilePath(`~/test.log`) error = %v", err)
		return
	}
	f, err := gofastogt.InitLogFile(*pathFile, maxFileSize)
	if err != nil {
		t.Errorf("InitLogFile(%v) error = %v", *pathFile, err)
		return
	}
	log.SetOutput(f)
	// fills the file more than the maximum
	for i := 0; i != -1; i++ {
		log.Println("Data")
		file, _ := f.Stat()
		if file.Size() > maxFileSize+50 {
			break
		}
	}
	err = f.Close()
	if err != nil {
		t.Errorf("file.Close error = %v", err)
		return
	}
	f, err = gofastogt.InitLogFile(*pathFile, maxFileSize)
	if err != nil {
		t.Errorf("InitLogFile(%v) error = %v", *pathFile, err)
		return
	}
	file, err := f.Stat()
	// checks the newly created file for size
	if file.Size() > maxFileSize {
		err = errors.New("invalid file size")
	}
	if err != nil {
		t.Errorf("Checks the newly created file for size error = %v", err)
		return
	}
	err = f.Close()
	if err != nil {
		t.Errorf("file.Close error = %v", err)
		return
	}
	err = os.Remove(*pathFile)
	if err != nil {
		t.Errorf("file.Remove error = %v", err)
		return
	}
}
