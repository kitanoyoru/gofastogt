package unittests

import (
	"reflect"
	"testing"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func TestGetHardwareHash(t *testing.T) {
	type args struct {
		exp_key gofastogt.ExpiredKey
	}
	tests := []struct {
		name    string
		args    args
		want    gofastogt.HardwareHash
		wantErr bool
	}{
		{
			name:    "case_1",
			args:    args{exp_key: []byte("006f68a6d700005eb12ee30000aa54e02c0180455b1af40340901c4ef60c70eea82cf5045044e891c129ef5a9fafd7e10")},
			want:    []byte("6f68a6d75eb12ee3aa54e02c455b1af4901c4ef6eea82cf544e891c15a9fafd70"),
			wantErr: false,
		},
		{
			name:    "Case_2",
			args:    args{exp_key: []byte("006a60acdb000052b821e30000ab50ea2a0190475117fc0510931440f1")},
			wantErr: true,
			want:    []byte(""),
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.GetHardwareHash(tt.args.exp_key)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetHardwareHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil {
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("GetHardwareHash() = %v, want %v", got, tt.want)
				}
			}

		})
	}
}

func TestGenerateHardwareHash(t *testing.T) {
	tests := []struct {
		name    string
		args    gofastogt.AlgoType
		wantErr bool
	}{
		{name: "case_1",
			args:    gofastogt.AlgoType(1),
			wantErr: false},
		{name: "case_2",
			args:    gofastogt.AlgoType(0),
			wantErr: false},
		{name: "case_3",
			args:    gofastogt.AlgoType(2),
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := gofastogt.GenerateHardwareHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateHardwareHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
