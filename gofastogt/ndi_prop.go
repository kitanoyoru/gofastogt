package gofastogt

import (
	"encoding/json"
	"errors"
)

type NDIProp struct {
	NdiName string `json:"ndi_name"`
}

func NewNDIProp(name string) *NDIProp {
	return &NDIProp{
		NdiName: name,
	}
}

func (p *NDIProp) Equals(other *NDIProp) bool {
	return p.NdiName == other.NdiName
}

func (p *NDIProp) UnmarshalJSON(data []byte) error {
	req := struct {
		NdiName *string `json:"ndi_name"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.NdiName == nil {
		return errors.New("ndi_name field required")
	}

	p.NdiName = *req.NdiName

	return nil
}
