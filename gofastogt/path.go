package gofastogt

import (
	"os/user"
	"path/filepath"
)

func preparePath(path string) (*string, error) {
	if len(path) == 0 || (path[0] != '~' && path[0] != '/') {
		return nil, ErrInvalidInput
	}
	if path[0] == '/' {
		return &path, nil
	}
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	path = filepath.Join(usr.HomeDir, path[1:])
	return &path, nil
}

func StableFilePath(path string) (*string, error) {
	return preparePath(path)
}

func StableDirPath(path string) (*string, error) {
	stableDir, err := preparePath(path)
	if err != nil {
		return nil, err
	}
	if (*stableDir)[len(*stableDir)-1] != '/' {
		*stableDir += "/"
	}
	return stableDir, nil
}

type FilePathProtocol struct {
	path string
}

func MakeFilePathProtocol(path string) (*FilePathProtocol, error) {
	stabled, err := StableFilePath(path)
	if err != nil {
		return nil, err
	}

	return &FilePathProtocol{*stabled}, nil
}

func (file *FilePathProtocol) GetFilePath() string {
	return "file://" + file.path
}

func (file *FilePathProtocol) GetPath() string {
	return file.path
}
